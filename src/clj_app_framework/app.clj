(ns clj-app-framework.app
  (:require [taoensso.timbre :as log])
  (:import (clojure.lang ExceptionInfo)))

(defmacro task-decorator
  "Ensure task exceptions are handled and always return vector of size 2."
  [f]
  `(try
     (let [result# ~f]
       (if (and (vector? result#) (= 2 (count result#)))
         result#
         [result# nil]))
     (catch Exception e#
       [nil e#])))

(defn exec
  [options-map {:keys [registry-map system-fn]}]
  (try
    (let [{:keys [func sysp]} (get registry-map (:action options-map))]
      ;; Set runtime log-level
      (log/set-level! (:log-level options-map))
      ; Connection pooling w/ hikari-cp is common. Make less noisy.
      (when (not= (:log-level options-map) :trace)
        (log/merge-config! {:ns-blacklist ["com.zaxxer.hikari.*"]}))
      ; execute task
      (-> (system-fn {:sysp sysp})
          (apply [func])
          task-decorator))
    (catch ExceptionInfo e
      [nil e])
    (catch Exception e
      [nil (str e)])))