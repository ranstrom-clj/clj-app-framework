(ns clj-app-framework.sysutils)

(defn closeable
  ([value]
   (closeable value identity))
  ([value close]
   (reify
     clojure.lang.IDeref
     (deref [_] value)
     java.io.Closeable
     (close [_] (close value)))))