(ns clj-app-framework.cli
  (:require [clojure.pprint :as pprint]
            [clojure.string :as string]
            [clojure.tools.cli :as tools])
  (:import (clojure.lang ExceptionInfo)))

(def log-levels-set #{:warn :info :error :debug :trace})

(def log-levels (->> log-levels-set (map name) vec (string/join ", ")))

(defn- argument-set
  "Return set of arguments."
  [registry-map]
  (reduce
   (fn [key-names key]
     (conj key-names (name key)))
   #{}
   (keys registry-map)))

(defn- get-max-key-size
  "From a vector of keywords, return the maximum character length."
  [ks]
  (->> ks
       (reduce
        (fn [key-sizes key]
          (set (conj key-sizes (count (name key)))))
        [])
       (apply max)))

(defn- get-cli-actions-summary
  "Return line-delimited string of CLI actions as `action <spaces> desc`"
  [registry-map]
  (let [sorted-registry-keys (sort (keys registry-map))
        padded-max-key-size (+ 5 (get-max-key-size (keys registry-map)))]
    (->> sorted-registry-keys
         (reduce
          (fn [cli-actions reg-key]
            (let [space-length (- padded-max-key-size (count (name reg-key)))
                  cli-action (str "  " (name reg-key)
                                  (string/join "" (repeat space-length " "))
                                  (-> registry-map reg-key :desc))]
              (conj cli-actions cli-action)))
          [])
         (string/join "\n"))))

(defn cli-options [add-cli-options]
  (concat
   add-cli-options
   [["-l" "--log-level LOG-LEVEL" (format "Logging level (%s)" log-levels)
     :default :info
     :parse-fn keyword]
    ["-h" "--help"]]))

(defn- usage [options-summary {:keys [app-header app-abbrev registry-map]
                               :or {app-header "" app-abbrev ""}}]
  (string/join
   \newline
   [app-header
    ""
    (format "Usage: %s [options] action" app-abbrev)
    ""
    "Options:"
    options-summary
    ""
    "Actions:"
    (get-cli-actions-summary registry-map)
    ""
    "Please refer to repository for more details."]))

(defn error-msg [errors]
  (str "The following errors occurred:\n\n"
       (string/join \newline errors)))

(defn validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args {:keys [registry-map add-cli-options] :as cli-opts}]
  (let [{:keys [options arguments errors summary]}
        (tools/parse-opts args (cli-options add-cli-options))]
    (cond
      ; help => exit OK with usage summary
      (:help options) {:exit-message (usage summary cli-opts) :ok? true}
      ; errors => exit with description of errors
      errors {:exit-message (error-msg errors)}
      ;; custom validation on arguments
      (and (= 1 (count arguments))
           ((argument-set registry-map) (first arguments)))
      {:action (first arguments) :options options}
      ; failed custom validation => exit with usage summary
      :else {:exit-message (usage summary cli-opts)})))

(defn print-msg [msg]
  (-> msg
      (#(cond
          (and (= (type %) ExceptionInfo)
               (:validate-error (ex-data %))) (-> % (ex-data) :validate-error)
          (= (type %) ExceptionInfo) (ex-data %)
          (and (map? %) (:validate-error %)) (:validate-error %)
          :else %))
      (#(if (string? %) (println %) (pprint/pprint %)))))

(defn exit [status msg]
  (print-msg msg)
  (System/exit status))

(defn task-return
  [[return errors]]
  (if (nil? errors)
    (exit 0 (or return "Execution Success"))
    (exit 1 errors)))

(defn parse-cli
  [args cli-opts]
  (let [{:keys [action options exit-message ok?]} (validate-args args cli-opts)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (merge {:action (keyword action)} options))))