(ns clj-app-framework.cli-test
  (:require [clj-app-framework.cli :as cli]
            [clojure.test :refer [deftest is testing]]))

(defn sample-task [{:keys [success]}]
  (if success
    ["YEAH!" nil]
    [nil "NO!"]))

(def registry-map
  {:dostuff {:func sample-task}
   :doother {:func sample-task}})

(def cli-opts
  {:app-header      "My App"
   :app-abbrev      "ma"
   :registry-map    registry-map
   :add-cli-options [["-p" "--properties PROPERTIES" "Properties filepath"
                      :default "properties.json"]]})

(deftest test-cli-parsing
  (testing "Simple cli-parsing"
    (with-redefs [cli/exit (fn [status _] status)]
      (is (= (cli/parse-cli ["dostuff" "-l" "debug"] cli-opts)
             {:properties "properties.json"
              :action     :dostuff
              :log-level  :debug}))
      (is (= (cli/parse-cli ["dostuff" "-p" "myprop.json"] cli-opts)
             {:properties "myprop.json"
              :action     :dostuff
              :log-level  :info}))
      (is (= 1 (cli/parse-cli ["dostuff" "-w" "something"] cli-opts)))
      (is (= 0 (cli/parse-cli ["-h"] cli-opts)))
      (is (= 1 (cli/parse-cli ["donothing"] cli-opts))))))

(deftest test-print-msg
  (testing "CLI print message"
    (is (nil? (cli/print-msg (ex-info "Error" {:validate-error "err1"}))))
    (is (nil? (cli/print-msg (ex-info "Error" {:error "Some Error"}))))
    (is (nil? (cli/print-msg {:validate-error "Some error"})))
    (is (nil? (cli/print-msg "Looks good!")))
    (is (nil? (cli/print-msg {:success "Also good!"})))))